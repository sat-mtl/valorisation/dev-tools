"""
This script is handling the CHANGELOG changes across all our projects
with some other tricks. It is able to:

- Determine the current version of any project (C++, Python, JS)
- Parse any CHANGELOG and add new commit according to the latest tag
- Adjust commit and tags with CLI arguments
- Ask to user the change of the version (MAJOR, MINOR, PATCH, CANDIDATE)
- Remove the intermediate changes in the CHANGELOG during candidate cycle
- Commit the new changes with a standard commit message
- Build the project by ensuring all project config is up-to-date
    - it changes the package.json version and rebuilt it
    - it changes the CMakeLists.txt version
- Logs every operations in order to troubleshoot issues
- TODO: Tag the new changes with the name of the version

In addition, this script is mixing
- All the previous script from Valorisation `update-changelog.sh`
- the Switcher and Splash's release scripts
- And the CHANGELOG standards like:
    - https://keepachangelog.com/en/1.0.0/
    - https://common-changelog.org/
    - https://github.com/conventional-changelog/conventional-changelog
"""

import re
import argparse
import os

from datetime import datetime
from dataclasses import dataclass, field
from subprocess import check_output
from typing import List
from enum import IntEnum, unique
from glob import glob
from copy import deepcopy

import logging

logging.basicConfig(encoding="utf-8", level=logging.INFO)

PRETTY_SEP = "----"


class Git:
    """Static methods that helps to get git statuses."""

    @property
    def tags(self):
        """Gets the most recent git tag of the project."""
        cmd = "git tag"
        return check_output(cmd, shell=True, encoding="utf-8")

    @property
    def latest_tag(self):
        """Gets the most recent git tag of the project."""
        cmd = "git describe --tags --abbrev=0"
        return check_output(cmd, shell=True, encoding="utf-8")

    def get_tag_date(self, tag: str):
        """Gets the date of a git tag."""
        cmd = f"git log -1 --format=%ai {tag}"
        return check_output(cmd, shell=True, encoding="utf-8")

    def get_logs_from_tag(self, tag: str):
        """Gets all the commit made after the most recent git tag."""
        cmd = (
            f'git log --pretty="{PRETTY_SEP}%n%t %b"'
            f'        --since="{self.get_tag_date(tag)}"'
        )
        return check_output(cmd, shell=True, encoding="utf-8")

    @property
    def latest_tag_date(self):
        """Gets the date of the most recent git tag."""
        return self.get_tag_date(self.latest_tag)

    @property
    def logs(self):
        """Gets all the commit made after the most recent git tag."""
        cmd = (
            f'git log --pretty="{PRETTY_SEP}%n%t %b"'
            f'        --since="{self.git_latest_tag_date}"'
        )
        return check_output(cmd, shell=True, encoding="utf-8")

    def add(self, files):
        """Stages new files"""
        add_cmd = f'git add {" ".join(files)}'
        return check_output(add_cmd, shell=True, encoding="utf-8")

    def commit(self, message):
        """Commits a message"""
        commit_cmd = f'git commit -m "{message}"'
        check_output(commit_cmd, shell=True, encoding="utf-8")

    def tag(self, tag_name):
        """Creates a new tag

        Parameters
        ----------
        tag_name : str
            Name of the new tag.
        """
        tag_cmd = f"git tag {tag_name}"
        check_output(tag_cmd, shell=True, encoding="utf-8")


git = Git()


@unique
class VersionIncrease(IntEnum):
    """
    Version increase type
    """

    MAJOR = 1
    MINOR = 2
    PATCH = 3
    CANDIDATE = 4


@dataclass
class Version:
    """Represents the semantic version number."""

    major: int = 0
    minor: int = 0
    patch: int = 0
    candidate: int = None

    @staticmethod
    def from_str(version_str):
        """Parses the Version model from a string.

        Parameters
        ----------
        version_str : str
            A version as a string.
        """
        if match := re.search(r"(\d+)\.(\d+).(\d+)(-rc(\d+))?", version_str):
            major = int(match[1])
            minor = int(match[2])
            patch = int(match[3])
            candidate = None

            if match.lastindex == 4:
                candidate = int(match[5])

        return Version(major, minor, patch, candidate)

    @staticmethod
    def from_branch():
        """Parses the Version model from the git branch."""
        cmd = "git rev-parse --abbrev-ref HEAD"
        branch = check_output(cmd, shell=True, encoding="utf-8")

        version = Version.from_str(branch.strip().split("/")[-1])
        logging.info(f'Finds version "{version}" from the branch "{branch}"')

        return version

    @property
    def without_candidate(self):
        return Version(self.major, self.minor, self.patch, None)

    def __eq__(self, other):
        """Checks if two Version models are equals.

        Parameters
        ----------
        other : Version
            The other version to compare.
        """
        return (
            self.major == other.major
            and self.minor == other.minor
            and self.patch == other.patch
        )

    def __lt__(self, other):
        """Checks if a version is lower than another version.

        Parameters
        ----------
        other : Version
            The other version to compare.
        """
        if self.major != other.major:
            return self.major < other.major

        if self.minor != other.minor:
            return self.minor < other.minor

        if self.patch != other.patch:
            return self.patch < other.patch

        if self.candidate is None and other.candidate is not None:
            return False
        elif self.candidate is not None and other.candidate is None:
            return True
        else:
            return self.candidate < other.candidate

    def __str__(self):
        """Gets the version as a string"""
        version = f"{self.major}.{self.minor}.{self.patch}"

        if self.candidate is not None:
            version += f"-rc{self.candidate}"

        return version

    def increment(self, increment: VersionIncrease = None):
        """Increments interactively the version

        Parameters
        ----------
        increment : VersionIncrease
            Skips the input increment if set
        """
        major = self.major
        minor = self.minor
        patch = self.patch
        candidate = self.candidate

        if not increment:
            choice = input(
                "Choose the right version increment:\n"
                + f"\t{VersionIncrease.MAJOR}) Major release\n"
                + f"\t{VersionIncrease.MINOR}) Minor release\n"
                + f"\t{VersionIncrease.PATCH}) Patch release\n"
                + f"\t{VersionIncrease.CANDIDATE}) Candidate release\n"
                + "This will impact the new version number"
                + "(W.X.Y-rcZ matches the choices 1.2.3.4):"
            )
            increment = int(choice)

        if increment == VersionIncrease.MAJOR:
            major, minor, minor, candidate = (
                self.major + 1,
                0,
                0,
                None,
            )
        elif increment == VersionIncrease.MINOR:
            minor, patch, candidate = (self.minor + 1, 0, None)
        elif increment == VersionIncrease.PATCH:
            patch, candidate = (self.patch + 1, None)
        elif increment == VersionIncrease.CANDIDATE:
            if self.candidate is not None:
                candidate = self.candidate + 1
            else:
                candidate = 0

        version = Version(major, minor, patch, candidate)
        logging.info(f'Bumps version from "{self}" to "{version}"')

        return version


@dataclass
class MergeRequest:
    """Represents a Gitlab merge request link."""

    link: str = ""

    @property
    def number(self):
        """Gets the merge request number."""
        return self.link.split("!")[-1]

    @property
    def url(self):
        """Gets the merge request URL."""
        return "https://gitlab.com/" + self.link.replace(
            "!", "/-/merge_requests/"
        )

    @property
    def ref(self):
        """Gets the merge request reference number."""
        return "!" + self.number

    @staticmethod
    def from_commit(commit):
        """Creates a MergeRequest object from a Commit.

        Parameters
        ----------
        commit : Commit
            A commit model that has a reference to a merge request.
        """
        mr = None

        for line in commit.body:
            if match := re.search("^See merge request (.*)$", line):
                mr = MergeRequest(match[1])

        return mr


@dataclass
class Commit:
    """Represents a git commit."""

    tree_hash: str = ""
    title: str = ""
    body: List[str] = field(default_factory=list)

    def is_empty(self) -> bool:
        """Checks if the commit is empty."""
        return self.title == "" or self.tree_hash == ""

    def is_release(self) -> bool:
        """Checks if the commit is a release commit."""
        return bool(re.search(r"[Rr]elease", self.title))

    def is_merge(self) -> bool:
        """Checks if the commit is a merge commit."""
        return bool(re.search(r"[Mm]erge", self.title))

    @property
    def merge_request(self) -> bool:
        """Gets the merge request reference of the commit."""
        return MergeRequest.from_commit(self)

    @staticmethod
    def from_logs(logs: str) -> list:
        """Creates list of Commit models from all git logs

        Parameters
        ----------
        logs : str
            All pretty logs produced by the `git log` command.
        """
        commits = []

        for log in logs.split(PRETTY_SEP):
            commits.append(Commit.from_log(log))

        return [commit for commit in commits if not commit.is_empty()]

    @staticmethod
    def from_log(log):
        """Creates a Commit model from an entry of the git log

        Parameters
        ----------
        log : str
            An entry from the git log
        """
        tree = ""
        title = ""
        body = []

        for line in log.split("\n"):
            if match := re.search(r"^(?P<tree>\S{7,8})\b(?P<title>.*)$", line):
                tree = match.group("tree")
                title = match.group("title").strip()
            else:
                body.append(line.strip())

        logging.debug(f"Add a new commit: {title}")

        return Commit(tree, title, body)

    def to_md(self) -> str:
        """Convert the Commit model to a markdown line."""
        if mr := self.merge_request:
            return f"{self.title} ([{mr.ref}]({mr.url}))"
        else:
            return self.title

    def __str__(self) -> str:
        """String representation of the Commit model."""
        return self.to_md()

    def __repr__(self) -> str:
        """Debugging representation of the Commit model."""
        return f"{self.title}"


@dataclass
class Changelog:
    """Represents the full CHANGELOG file."""

    software_name: str
    changelog_path: str
    old_changelog: dict[str, list] = field(default_factory=dict)
    previous_tag: str | None = None

    @staticmethod
    def from_old(
        software_name: str,
        changelog_path: str,
        previous_tag: str | None = None,
    ):
        """Creates a Changelog object by parsing the CHANGELOG
        into a dict by versions.

        Parameters
        ----------
        name : str
          Name of the program.
        changelog_path : str
          Path to the CHANGELOG.
        """
        step = "intro"
        changelog = {}
        changelog[step] = []

        with open(changelog_path) as f:
            content = f.readlines()

            for line in content:
                if match := re.search(
                    rf"^({software_name}) (\d+\.\d+\.\d+(-rc\d+)?)",
                    line,
                ):
                    step = match[2]
                    changelog[step] = []

                changelog[step].append(line)

        changelog = Changelog(
            software_name, changelog_path, changelog, previous_tag
        )
        logging.info(f"Parsed current Changelog from {changelog_path}")
        logging.info(f"Changelog entries: {len(changelog.old_changelog)}")

        return changelog

    def clean_candidates(self):
        """Cleans the candidate logs"""
        logging.info("Cleans all the candidate versions")
        rc_keys = [key for key in self.old_changelog.keys() if "rc" in key]

        for key in rc_keys:
            self.old_changelog.pop(key, None)

    @property
    def new_commits(self) -> list:
        """Gets all the commit models after the most recent git tag."""
        git_logs = None
        commits = None

        if git.tags == "":
            git_logs = git.logs
        else:
            commits = Commit.from_logs(
                git.get_logs_from_tag(self.previous_tag)
            )

        if git_logs:
            commits = Commit.from_logs(git_logs)

        return commits

    @property
    def new_entries(self) -> list:
        """Gets the new CHANGELOG entries by excluding release
        and merge commits."""
        entries = []

        for commit in self.new_commits:
            if not commit.is_release() and not commit.is_merge():
                entries.append(commit)

        return entries

    @staticmethod
    def default_title() -> str:
        """Gets the default title of a CHANGELOG"""
        return "Release Notes\n" + "==================="

    def generate_new_title(self, new_version):
        """Gets the title of the new CHANGELOG entry in markdown."""
        today = datetime.today().strftime("%Y-%m-%d")
        return [
            f"{self.software_name} {new_version} ({today})\n",
            "---------------------------------\n\n",
        ]

    def generate_new_changelog(self, new_version, previous_tag):
        """Gets the latest CHANGELOG entry."""
        new_entry = (
            self.generate_new_title(new_version)
            + [f" * {str(commit)}\n" for commit in self.new_entries]
            + ["\n"]
        )

        new_changelog = deepcopy(self.old_changelog)
        new_changelog[str(new_version)] = new_entry
        changelog = Changelog(
            self.software_name,
            self.changelog_path,
            new_changelog,
            previous_tag,
        )

        logging.info(f'Generates new changelog for "{new_version}"')
        logging.info(f"Changelog entries: {len(changelog.old_changelog)}")

        return changelog

    def to_md(self):
        """Gets the full CHANGELOG in markdown."""
        ch = deepcopy(self.old_changelog)

        if ch["intro"]:
            intro = ch["intro"]
            del ch["intro"]
        else:
            intro = Changelog.default_title()

        sorted_ch = dict(reversed(sorted(ch.items())))
        full = intro

        for content in sorted_ch.values():
            full += content

        return "".join(full).strip()

    def __str__(self):
        return self.to_md()

    def write(self):
        """Replaces the current CHANGELOG with the new one."""
        with open(self.changelog_path, "w") as file:
            file.write(self.to_md())


@unique
class ProjectType(IntEnum):
    """Project type enum"""

    JS = 0
    Cpp = 1
    Py = 2

    def __str__(self):
        return f"{self.name}"


class Project:
    """Represent the project configuration"""

    def __init__(self):
        logging.info(f'Prepares release for project "{self.current_name}"')
        logging.info(f'Project type is "{self.types[0]}"')
        logging.info(f'Current version is "{self.current_version}"')

    @property
    def current_name(self):
        """Gets the current project name from the git URL"""
        cmd = "git config --local remote.origin.url"
        out = check_output(cmd, shell=True, encoding="utf-8")
        name = None

        if match := re.search(r".*/([^.]*).git", out):
            name = match[1]

        return name

    @property
    def changelog_path(self):
        """Gets the relative path of the CHANGELOG"""
        root_files = glob("*")
        path = None

        if "CHANGELOG.md" in root_files:
            path = "CHANGELOG.md"
        elif "CHANGELOG" in root_files:
            path = "CHANGELOG"
        elif "NEWS.md" in root_files:
            path = "NEWS.md"

        return path

    @property
    def version_file_paths(self):
        """Gets the relative paths of version files"""
        root_files = glob("*")

        patterns = [
            "package.json",
            "CMakeLists.txt",
            "pyproject.toml",
        ]
        paths = []

        for pattern in patterns:
            if pattern in root_files:
                paths.append(pattern)

        return paths

    @property
    def types(self):
        """Gets all the types of the project according to its root files."""
        types = []
        root_files = glob("*")

        if "package.json" in root_files:
            types.append(ProjectType.JS)

        if "CMakeLists.txt" in root_files:
            types.append(ProjectType.Cpp)

        if "pyproject.toml" in root_files:
            types.append(ProjectType.Py)

        return types

    @property
    def current_version(self):
        """Gets the current version of the project."""
        if self.types[0] == ProjectType.JS:
            with open("package.json") as f:
                content = "\n".join(f.readlines())
                version = re.search(r'"version": "(.*)"', content)[1]
        elif self.types[0] == ProjectType.Cpp:
            with open("CMakeLists.txt") as f:
                content = "\n".join(f.readlines())

                if match := re.search(
                    rf"{self.current_name} VERSION ([\d\.]+)", content
                ):
                    version = match[1]
                elif match := re.search(r"VERSION_MAJOR (\d+)", content):
                    major = match[1]
                    minor = re.search(r"VERSION_MINOR (\d+)", content)[1]
                    patch = re.search(r"VERSION_PATCH (\d+)", content)[1]
                    version = f"{major}.{minor}.{patch}"

        elif self.types[0] == ProjectType.Py:
            with open("pyproject.toml") as f:
                content = "\n".join(f.readlines())
                version = re.search(r'(__)?version(__)? = "(.*)"', content)[3]

        logging.info(f"Detected current version: {version}")
        return Version.from_str(version)

    @property
    def current_changelog(self):
        """Gets the current changelog"""
        return Changelog.from_old(
            self.current_name,
            self.current_version,
            self.changelog_path,
        )

    def replace_version(self, file_path, version_pattern, replace_pattern):
        """Replaces a pattern in a file in the project.

        Parameters
        ----------
        file_path : str
            Path of the file to update.
        version_pattern : regex
            Pattern to replace.
        replace_pattern : str
            The value to replace by.
        """
        if os.path.exists(file_path):
            data = ""

            with open(file_path, "r") as file:
                data = file.read()

            data = re.sub(version_pattern, replace_pattern, data)

            with open(file_path, "w") as file:
                file.write(data)

    def build(self):
        """Builds the project according to its type."""
        if ProjectType.JS in self.types:
            check_output("npm install", shell=True, encoding="utf-8")

    def write(self, new_version, changelog):
        """Writes the updated configuration"""
        if ProjectType.JS in self.types:
            self.replace_version(
                "package.json",
                r'"version": "(.*)"',
                f'"version": "{new_version}"',
            )

        if ProjectType.Cpp in self.types:
            major = new_version.major
            minor = new_version.minor
            patch = new_version.patch

            self.replace_version(
                "CMakeLists.txt",
                rf"{self.current_name} VERSION ([\d\.\-]+)",
                f"{self.current_name} VERSION {new_version.without_candidate}",
            )

            self.replace_version(
                "CMakeLists.txt",
                rf"{self.current_name.upper()}_VERSION_MAJOR (\d)",
                f"{self.current_name.upper()}_VERSION_MAJOR {major}",
            )

            self.replace_version(
                "CMakeLists.txt",
                rf"{self.current_name.upper()}_VERSION_MINOR (\d)",
                f"{self.current_name.upper()}_VERSION_MINOR {minor}",
            )

            self.replace_version(
                "CMakeLists.txt",
                rf"{self.current_name.upper()}_VERSION_PATCH (\d)",
                f"{self.current_name.upper()}_VERSION_PATCH {patch}",
            )

        changelog.write()

    def commit(self, new_version):
        git.add([self.changelog_path] + self.version_file_paths)
        git.commit(f"🚩 Update CHANGELOG and version for release {new_version}")

    def tag(self, new_version):
        """Tags the project"""
        logging.info(f"TODO: tag the project with name {new_version}")


def release(
    candidate=False,
    final=False,
    init=False,
    no_commit=False,
    previous_tag=None,
):
    """Handles a release of a project.

    Parameters
    ----------
    candidate : bool
      Flag for a candidate release.
    final : bool
      Flag for a final release (strip candidate logs).
    init : bool
      First release of a project (don't check tags).
    no_commit : bool
      Prevent commiting the result of this script.
    previous_tag : str | None
      Force the script to check a specific tag.
    """
    project = Project()

    prev_tag = previous_tag

    if not prev_tag:
        prev_tag = git.latest_tag

    logging.info(f"Will generate new CHANGELOG from tag {prev_tag}")

    old_changelog = Changelog.from_old(
        project.current_name, project.changelog_path, prev_tag
    )
    increment = None

    if candidate:
        logging.info("Will only update the candidate version")
        increment = VersionIncrease.CANDIDATE
    elif final:
        old_changelog.clean_candidates()

    if init:
        version = Version()
    if final:
        version = project.current_version
    else:
        version = project.current_version.increment(increment)

    new_changelog = old_changelog.generate_new_changelog(version, prev_tag)

    project.write(version, new_changelog)
    project.build()

    if not no_commit:
        project.commit(version)
        project.tag(version)

    logging.info(
        f"The project {project.current_name} is updated to version {version}!"
    )


if __name__ == "__main__":
    cli = argparse.ArgumentParser(
        description="Release a project under a new version"
    )

    cli.add_argument(
        "--previous-tag", help="Previous release tag", default=None
    )
    cli.add_argument(
        "--no-commit",
        action="store_true",
        help="Prevent commiting new changelog",
    )

    exclusive = cli.add_mutually_exclusive_group()
    exclusive.add_argument(
        "--candidate",
        action="store_true",
        help="Increments the candidate of the project version",
    )

    exclusive.add_argument(
        "--final",
        action="store_true",
        help="Cleans the candidate of the project version",
    )

    exclusive.add_argument(
        "--init",
        action="store_true",
        help="Initializes a project without tags",
    )

    args = cli.parse_args()
    release(
        args.candidate,
        args.final,
        args.init,
        args.no_commit,
        args.previous_tag,
    )
