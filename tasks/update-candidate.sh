#!/bin/bash

# Updates the candidate branch of a project.
# Arguments:
#   Name of the released project.
#   Version of the release.

updateCandidate () {
  local name="$1"
  local version="$2"

  curl -sL "$DEV_TOOLS/tasks/update-authors.sh" | bash
  curl -sL "$DEV_TOOLS/tasks/update-changelog.sh" | bash -s "$name" "$version"
  curl -sL "$DEV_TOOLS/tasks/update-version.sh" | bash -s "$version"
}

if [ "$#" -ne "2" ]; then
  echo "NAME and VERSION arguments are required"
  exit 0
else
  updateCandidate "$1" "$2"
fi
