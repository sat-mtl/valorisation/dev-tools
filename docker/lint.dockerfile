FROM debian:bookworm-slim
LABEL MAINTAINER="Scenic team <scenic-dev@sat.qc.ca>"
ENV DEBIAN_FRONTEND noninteractive

COPY .pre-commit-config.yaml /bootstrap-pre-commit-cache/.pre-commit-config.yaml

RUN apt-get update -y --no-install-recommends \
    # Install linters
    && apt-get install -y -qq \
        git \
        python3 \
        clang-format \
        pre-commit \
    # Clean apt cache
    && apt-get clean \
    && apt-get autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN cd /bootstrap-pre-commit-cache \
   && git init \
   # Bootstrap pre-commits hooks by downloading them
   && pre-commit run --all-files
