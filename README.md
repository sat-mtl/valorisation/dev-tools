# dev-tools

A collection of configuration files and utility tools for telepresence developers.

Before executing any documented commands, we recommand exporting the URL of this repository in your shell:

```bash
export DEV_TOOLS="https://gitlab.com/sat-mtl/valorisation/dev-tools/-/raw/main"
```

## Projects

All **front-end** projects are developped in **JavaScript / ECMAScript 12** with **NodeJS 10**. The **back-end** projects are developped in **Python 3.8**, **NodeJS 16** and **C++**.

All tools introduced here are about *telepresence* projects developed at the **Société des Arts Technologiques (SAT)**:

- Front-End
  - scenic (Javascript / SCSS)
  - ui-components (Javascript / SCSS)
  - Telluriq (Python)
- Back-End
  - scenic-core (Javascript)
  - node-switcher (Javascript / C++)
  - Switcher (C++)
  - Shmdata (C++)
  - NDI2shmdata (C++)
  - *DBoxer (C++)
- Configuration
  - *Scenic Station (Ansible / Yaml)
  - Development Tools (Shell)
  - ScenicOS (dockerfile)

All repositories preceded by a \* are closed-source and accessible only by SAT developers.

## Dependencies

### Common

In order to use the `dev-tools`, you must install [dialog](https://linux.die.net/man/1/dialog) and [curl](https://curl.se/).

```bash
sudo apt install dialog curl
```

These scripts are usable in MacOS but you should [update bash](https://apple.stackexchange.com/questions/193411/update-bash-to-version-4-0-on-osx) with [homebrew](https://brew.sh/).

```bash
brew install curl dialog bash
```

### NodeJS

You should install [volta](https://volta.sh/) in order to easily manage the **NodeJS** versions on your computer.

```sh
# install volta into home dotfiles
curl https://get.volta.sh | bash

# if automatic installer failed
export PATH="/home/$USER/.volta/bin:$PATH"

# install NodeJS LTS
volta install node@lts

# install and pin specific version (see scenic-core)
cd scenic-core; volta pin node@10
```

### pip3 and pipenv

You should install [**pipenv**](https://pipenv.readthedocs.io/en/latest/) because it will painlessly let you manage your different **Python** versions using **virtualenvs**. In addition, all telepresence projects written in Python use this tool.

```sh
sudo apt install python3-pip
pip3 install pipenv # Ubuntu
brew install pipenv # MacOS
```

## 🎉 Initialize a repository

The `config-project.sh` script will initialize your repository by downloading all issue templates, all the git hooks and all the **default** linter configurations.

```bash
curl -Ls $DEV_TOOLS/tasks/config-project.sh | bash
```

This script is downloading and using the following scripts:

| Name                      | Usage  | Description                              |
|---------------------------|--------|------------------------------------------|
| `init-hooks.sh`           | Config | Configures all git hooks                 |
| `init-issue-templates.sh` | Config | Configures all the issue templates       |
| `init-linter-config.sh`   | Config | Configures all the linter configurations |

## ⚠ Hooks

All `pre-commit` hooks are handled by [pre-commit](https://pre-commit.com/). They are compatible with any project that has a `.pre-commit-config.yaml` file at its root.

```bash
pip install pre-commit # installs pre-commit from pip
apt install pre-commit # installs pre-commit from apt

pre-commit install # install all pre-commit hooks
pre-commit uninstall # uninstall all pre-commit hooks

pre-commit run --all-files # runs the installed hooks on every files
```

### 🚨 Linters and formaters

For all development tasks we **recommand** to use linters and auto-formaters in order to harmonize our coding styles. So, for each language you use, you'll need to install a linter. Choose your pills:

#### Languages

##### 🕺🏽 [Javascript](https://developer.mozilla.org/en-US/docs/Web/javascript)

We use [StandardJS](https://standardjs.com/) that doesn't require any configuration:

```bash
npm i -g standard
```

##### 🐍 [Python](https://www.python.org/)

For python, we use [flake8](https://flake8.pycqa.org/en/latest/) for linting and [autopep8](https://pypi.org/project/autopep8/) for auto-formating:

```bash
pip3 install flake8 autopep8

# get the common configuration
curl -s $DEV_TOOLS/config/tox.ini > tox.ini
```

##### 💄 [CSS](https://developer.mozilla.org/en-US/docs/Glossary/CSS) and [SCSS](https://sass-lang.com/)

For styling syntaxes we use [Stylelint](https://stylelint.io/):

```bash
npm i -D stylelint

# install extra-dependencies for SCSS
npm i -D stylelint-scss

# get the common configuration
curl -s $DEV_TOOLS/config/stylelint.json > .stylelintrc
```

##### 🌾 C and [C++](https://isocpp.org/)

We commonly use [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html) with the LLVM style for C++ code:

```bash
sudo apt install clang-format

# get the common configuration
curl -s $DEV_TOOLS/config/clang-format > .clang-format
```

##### 🐚 [Bash scripts](https://www.gnu.org/software/bash/)

We suggest that you use [ShellCheck](https://www.shellcheck.net/) so you'll write safier scripts:

```bash
sudo apt install shellcheck
```

You can consult the warnings at <https://github.com/koalaman/shellcheck/wiki/><error-code>.

##### 🐟 [Dockerfile](https://docs.docker.com/engine/reference/builder/)

Also, we suggest that you use [hadolint](https://github.com/hadolint/hadolint) for your dockerfiles:

```bash
docker run --rm -i hadolint/hadolint < Dockerfile
```

You can consult the warnings at <https://github.com/hadolint/hadolint/wiki/><error-code>.

#### Pre-Commit Hook

You should use the `lintAndFormat` task as the `pre-commit` hook: it will detect your staged files and will apply the correct linter on them.

```bash
curl -s $DEV_TOOLS/tasks/lint-and-format.sh > .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
```

This script is using the environment variable `$FORMATTING` that will control the script:

- `FORMATTING=A` will **A**utomatically commit the changed implied by the reformating
- `FORMATTING=I` will **I**gnore the changes implied by the reformatting
- `FORMATTING=M` will abort the commit until the changes are **M**anually applied

Optionnaly, you can use this script independently *anywhere*:

```bash
export FORMATTING=A
curl -s $DEV_TOOLS/tasks/lint-and-format.sh | bash
```

##### 💡 Per-project configurations

We recommend to use [direnv](https://direnv.net/) for per-project configurations:

```bash
sudo apt install direnv

# Create a .envrc file in your project
echo "FORMATTING=A" > .envrc
direnv allow .
```

### Pre-Push Hook

The `pre-push` hook is executed when you push your branch to its upstream branch. It will check the branch name, which needs to respect specific conventions :

- `fix/<branch name>` aims to fix a bug
- `feat/<branch name>` aims to add a new feature
- `doc/<branch name>` aims to change documentation
- `test/<branch name>` aims to add software tests (unit/integration)
- `intl/<branch name>` aims to add a localization update
- `candidate/<branch name>` aims to add a new candidate release

You should install by executing:

```bash
curl -s $DEV_TOOLS/tasks/check-branch-name.sh > .git/hooks/pre-push
chmod +x .git/hooks/pre-push
```

### Commit-Msg Hook

The `commit-msg` hook is executed after the commit message has been entered. It will validate your commit message's format and spelling:

```bash
curl -s $DEV_TOOLS/tasks/check-commit-msg.sh > .git/hooks/commit-msg
chmod +x .git/hooks/commit-msg
```

#### Format

The hook will analyze your message using four criterias:

1. The first line must be at least 10 characters long
2. The first line must be 50 characters long or less
3. The second line must be empty
4. The remaining lines must be word-wrapped so that they have a length of 72 characters or less

This is to ensure the consistent quality of commit messages, in accordance to the [50/72 formatting rule](http://www.midori-global.com/blog/2018/04/02/git-50-72-rule) and the [seven rules of a great commit message](https://chris.beams.io/posts/git-commit/).

#### Spell-checking

The `commit-msg` hook will ensure that your message is free of spelling errors by using [`aspell`](http://aspell.net/). Any detected spelling errors will be flagged in the terminal but will not stop the commit. If you want to correct any mistakes detected by `aspell`, you can run `git commit --amend` to edit your last commit's message (assuming you haven't pushed it to remote).

#### Commit Message Template

The `.gitmessage` file is a great template for your commit messages, that can be used to help you write useful commit messages. To use it, simply run the following commands:

```sh
    # from dev-tools/
    ln -f .gitmessage ~/.gitmessage
    git config --global commit.template ~/.gitmessage
```

The template will be applied to all of your future commits. Keep in mind comments (lines starting with `#`) are ignored by Git.

### Switcher Hooks

Hooks specific to Switcher can be set up by following the instructions in [Switcher's repository](https://gitlab.com/sat-metalab/switcher/-/blob/master/doc/contributing.md#coding-style).

## Editors

### All editors

An [**EditorConfig**](https://editorconfig.org/) file needs to be linked in the parent folder of all projects in order to respect the code conventions of the team (don't be afraid, it's not very heavy).

```sh
ln -s .editorconfig ../.editorconfig
```

If this file is linked at the top of all projects, all editors (*given they have the appropriate extensions/plugins*) will be aware of the code conventions.

### Visual Studio Code

[**Visual Studio Code**](https://code.visualstudio.com/) is used because it can be configurated easily and can handle a lot of IDE use cases without being too heavy.

### Install

```sh
# Ubuntu
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

nix-env -i code # NixOS
brew cask install code # MacOS
```

### Workspace configurations

A workspace has been pre-configurated with some debug recipes for all telepresence projects. If you clone this repository in the same parent folder containing the projects described here, you can generate your workspace configuration simply by linking it to the parent folder.

```sh
git clone git@gitlab.com:sat-mtl/telepresence/dev-tools.git
ln -s dev-tools/telepresence.code-workspace telepresence.code-workspace
code telepresence.code-workspace # Open your workspace
```

There are some recommended extensions : some of theses extensions are required for the debug recipes. They can be used for linting and formating.

### Debug configurations

All debug configurations allow you to insert breakpoints directly into **Visual Studio Code**. These recipes are :

- `Bibliolab Client (Chromium)`
- `Bibliolab Echo`
- `Bibliolab Server`
- `Scenic (Chromium)`
- `Scenic Core`
- `Scenic Core (Unit Tests)`
- `Node Switcher (lldb)`
- `Switcher (gdb)`

**Bibliolab Client** and **Scenic** recipes run in Chromium (since they are web clients.) **Bibliolab Server** and **Scenic Core** are servers : the client and the server needs to run in parallel if you want to debug the full stack. **Bibliolab Echo** just sends back the websockets commands, and is useful to debug the client without using a lot of ressources. Finally, **Scenic Core (Unit Tests)** can be used to debug all Scenic Core unit tests.

There are also two recipes (**Bibliolab** and **Scenic**) which run the server and the client in parallel.

**Node Switcher** recipe is for debugging the switcher addon for NodeJS.

#### Switcher recipe

**Switcher (gdb)** recipe is used to debug Switcher, this recipe is based on the [CPP Visual Studio Code Debug specifications](https://74th.github.io/vscode-debug-specs/cpp/). It was linked to a specific program but it can be changed : all possibilities are documented for the [Native VSCode debugger](https://github.com/WebFreak001/code-debug).

## 🔖 Release Scripts

This is a collection of scripts that should help the team in common tasks such as release.

| Name                  | Usage   | Description                                             |
|-----------------------|---------|---------------------------------------------------------|
| `update-authors.sh`   | Release | Generates all authors from git commits                  |
| `update-changelog.sh` | Release | Generate the changelog of a project from the latest tag |
| `update-version.sh`   | Release | Upgrade the software version strings                    |

There is also a script that rules all of them:

```bash
curl -s "$DEV_TOOLS/tasks/update-candidate.sh" | bash -s Telluriq 0.1.0
```

This script takes the following arguments:
1. The version of the release (ex: _Scenic 4.0.8-rc4_)
2. The name of the milestone (ex: _Scenic v4.0.8_)

### ✅ Release Checklist Example

To create a project release, two things always need to be done beforehand:

- [ ] Update the AUTHORS file
- [ ] Update the CHANGELOG file
- [ ] Update the version strings

### 🏗 Issue board builder

In order to use the builder you must install the [zx](https://github.com/google/zx) scripting tool. Additionnaly, you'll need to generate an API token for Gitlab with the `read_api` option.

```bash
curl -s "$DEV_TOOLS/tasks/build-issue-board.mjs" | zx - "21080505" "DBoxer v1.1"
```

This script takes the following arguments:
1. The ID of the project (ex: _2101220_)
2. The name of the milestone (ex: _Scenic v4.0.8_)

Also it requires some global variables:

+ PRIVATE_TOKEN as a Personal Gitlab API access token for `read_api`
